package oyster


package object OysterCardOperation{

  val MAXIMUM_FARE = BigDecimal(3.2)
  type Zone = Int

  case class Station(name: String, zones: List[Zone])

  object Holborn      extends Station("Holborn",      List(1))
  object EarlCourt    extends Station("Earl’s Court", List(1,2))
  object Wimbledon    extends Station("Wimbledon",    List(3))
  object Hammersmith  extends Station("Hammersmith",   List(2))
  object Chelsea      extends Station("Chelsea",   List(2))

  case class Trip(departureStation: Station, arrivalStation: Option[Station] = None, vehicleMode: VehicleMode )

  case class VehicleMode(name: String)
  object Bus  extends VehicleMode("Bus")
  object Tube extends VehicleMode("Tube")

  case class OysterCard(trips: List[Trip], balance: BigDecimal)

  def calculateFare(trip: Trip): BigDecimal = {

    val maybeFare = (for {
      arrivalStation        <- trip.arrivalStation
      departureStationZones = trip.departureStation.zones.sorted
      arrivalStationZones   = arrivalStation.zones.sorted
    } yield {

        val isDiffZoneJourneyWithMultipleZoneArrivalStation: Boolean = arrivalStationZones != departureStationZones && departureStationZones.size == 1

        arrivalStationZones.size match {
          case 0 => Some(MAXIMUM_FARE)
          case 1 if isDiffZoneJourneyWithMultipleZoneArrivalStation =>journeyFare.get((departureStationZones:::arrivalStationZones).toSet)
          case _ => journeyFare.get(determineMostFavourableZone(departureStationZones, arrivalStationZones).toSet)
        }

      }).flatMap(a => a)

    trip.vehicleMode match {
      case Bus  => if(trip.arrivalStation.isDefined) BigDecimal(1.80) else MAXIMUM_FARE
      case Tube => maybeFare.getOrElse(MAXIMUM_FARE)
    }

  }

  def determineMostFavourableZone(departureStationZones: List[Zone], arrivalStationZones: List[Zone]): List[Zone] = {

    val maybeCustomerFavouredZone = arrivalStationZones.find(a => departureStationZones.contains(a))
      .orElse(arrivalStationZones.find(a => departureStationZones.exists(_ <= a)))

    maybeCustomerFavouredZone.map(List(_)).getOrElse(departureStationZones:::arrivalStationZones)
  }

  def deductFarefromBalance(trip: Trip, balance: BigDecimal) = balance - calculateFare(trip)

  def inwardBarrierCheckIn(oysterCard: OysterCard, station: Station, vehicleMode: VehicleMode): OysterCard  = {
    val uncompletedTrip = Trip(departureStation = station, vehicleMode = vehicleMode)
    oysterCard.copy(trips = oysterCard.trips:+uncompletedTrip, balance =oysterCard.balance - MAXIMUM_FARE)
  }

  def outwardBarrierCheckOut(trip: Trip, station: Station, balance: BigDecimal): (Trip, BigDecimal) = {
    val completedTrip = trip.copy(arrivalStation = Some(station))
    val finalBalance = deductFarefromBalance(completedTrip, balance + MAXIMUM_FARE)
    (completedTrip, finalBalance)
  }

  def outwardBarrierCheckOut2(oysterCard: OysterCard, station: Station): OysterCard = {

    oysterCard.trips.lastOption
      .map{ lastTrip =>
        val completedTrip = lastTrip.copy(arrivalStation = Some(station))
        val finalBalance = deductFarefromBalance(completedTrip, oysterCard.balance + MAXIMUM_FARE)

        val updatedTripList = oysterCard.trips.size match {
          case a if a == 0 => oysterCard.trips.updated(a, completedTrip)
          case _ => oysterCard.trips.dropRight(1):+completedTrip
        }

        oysterCard.copy(trips = updatedTripList, balance = finalBalance)
      }
      .getOrElse(oysterCard)
  }

  val journeyFare: Map[Set[Zone],BigDecimal] = Map(
    Set(1) -> BigDecimal(2.5),
    Set(2) -> BigDecimal(2.0),
    Set(3) -> BigDecimal(2.0),
    Set(1,2) -> BigDecimal(3.0),
    Set(2,3) -> BigDecimal(2.25),
    Set(1,3) -> BigDecimal(3.2),
    Set(1,2,3) -> BigDecimal(3.2)
  )

}