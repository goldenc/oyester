package oyester
import  oyster.OysterCardOperation._
import org.specs2.matcher.DataTables
import org.specs2.mutable.Specification

class OysterCardUnitSpec extends Specification with DataTables{

  "OysterCard" should {

    "charge the user the maximum fare when passing through the inward barrier [Bus]" in {
      val trip: Trip = Trip(departureStation = Holborn, vehicleMode = Bus )
      calculateFare(trip) must_== MAXIMUM_FARE
    }

    "charge the user the maximum fare when passing through the inward barrier [Tube]" in {
      val trip: Trip = Trip(departureStation = Holborn, vehicleMode = Tube )
      calculateFare(trip) must_== MAXIMUM_FARE
    }

    "determine the most favourable zone to favour lower fares for stations in two zones" in{
      def zoneTable =
        "departureStationZones" | "arrivalStationZones" | "favourableZone" |
          List(1)               !  List(1,2)            !  List(1) |
          List(2)               !  List(2,3)            !  List(2) |
          List(2,3)             !  List(2)              !  List(2) |
          List(1,2)             !  List(1)              !  List(1) |> {
          (a:List[Zone], b:List[Zone], c:List[Zone]) =>
            determineMostFavourableZone(a,b) must_== c
        }
      zoneTable
    }

    "determine a journey's fare given a boarding station and an arrival station" in {

      def tripTable =
        "departureStation" | "arrivalStation" | "vehicleMode = Tube" | "fare" |
          Holborn          ! Holborn          ! Tube                 ! BigDecimal(2.50) |  //Anywhere in Zone 1
          Wimbledon        ! Wimbledon        ! Tube                 ! BigDecimal(2.00) | //Any one zone outside zone 1
          Holborn          ! Hammersmith      ! Tube                 ! BigDecimal(3.00) | //Any two zones including zone 1
          Holborn          ! EarlCourt        ! Tube                 ! BigDecimal(2.50) | //Anywhere in Zone 1 multiple zone station
          EarlCourt        ! Holborn          ! Tube                 ! BigDecimal(2.50) | //Anywhere in Zone 1 multiple zone station
          Wimbledon        ! Hammersmith      ! Tube                 ! BigDecimal(2.25) | //Any two zones excluding zone 1
          Holborn          ! Wimbledon        ! Tube                 ! BigDecimal(3.20) | //Any three zones
          EarlCourt        ! Chelsea          ! Bus                  ! BigDecimal(1.80) |> { //Any bus journey
          (a:Station, b:Station, c:VehicleMode, d:BigDecimal) =>
            val trip: Trip = Trip(a, Some(b), c)
            calculateFare(trip) must_== d
        }

      tripTable
    }


    val testCard: OysterCard =  OysterCard(trips = List(), BigDecimal(30))

    "deduct the maximum fare from card when the user passes through the inward barrier at the station" in {
      val checkInCard =  inwardBarrierCheckIn(testCard, Holborn, Tube)
      val expectedCard = OysterCard(trips = List(Trip(departureStation = Holborn, vehicleMode = Tube)), balance = BigDecimal(30) - MAXIMUM_FARE)
      checkInCard must_== expectedCard
    }

    "recalculate the farw when the user passes through the outward barrier at the exit station" in {
      val checkInCard =  inwardBarrierCheckIn(testCard, Holborn, Tube)
      val (completedTrip, finalBalance) = outwardBarrierCheckOut(checkInCard.trips.last, Holborn, checkInCard.balance)
      completedTrip must_== checkInCard.trips.last.copy(arrivalStation = Some(Holborn))
      finalBalance must_== checkInCard.balance + MAXIMUM_FARE - BigDecimal(2.50)
    }

    //  Tube Holborn to Earl’s Court
    //  328 bus from Earl’s Court to Chelsea
    //  Tube Earl’s court to Hammersmith
    "demonstrate a user loading a card with £30, and taking the above trips, and then viewing the balance" in {
      var demoCard: OysterCard =  OysterCard(trips = List(), BigDecimal(30))

      //  Tube Holborn to Earl’s Court
      demoCard =  inwardBarrierCheckIn(demoCard, Holborn, Tube)
      demoCard = outwardBarrierCheckOut2(demoCard, EarlCourt)
      val expectedholbornToEarlCourtTrip = Trip(Holborn, Some(EarlCourt), Tube)
      val expectedHolbornToEarlCourtCard = OysterCard(trips = List(expectedholbornToEarlCourtTrip), BigDecimal(27.5))

      demoCard must_== expectedHolbornToEarlCourtCard

      //  328 bus from Earl’s Court to Chelsea
      demoCard =  inwardBarrierCheckIn(expectedHolbornToEarlCourtCard, EarlCourt, Bus)
      demoCard = outwardBarrierCheckOut2(demoCard, Chelsea)
      val expectedBusEarlCourtToChelseaTrip = Trip(EarlCourt, Some(Chelsea), Bus)
      val expectedBusEarlCourtToChelseaCard = OysterCard(trips = expectedHolbornToEarlCourtCard.trips:+expectedBusEarlCourtToChelseaTrip, BigDecimal(25.7))

      demoCard must_== expectedBusEarlCourtToChelseaCard

      //  Tube Earl’s court to Hammersmith
      demoCard =  inwardBarrierCheckIn(expectedBusEarlCourtToChelseaCard, EarlCourt, Tube)
      demoCard = outwardBarrierCheckOut2(demoCard, Hammersmith)
      val expectedTubeEarlCourtToHammersmithTrip = Trip(EarlCourt, Some(Hammersmith), Tube)
      val expectedTubeEarlCourtToHammersmithCard = OysterCard(trips = expectedBusEarlCourtToChelseaCard.trips:+expectedTubeEarlCourtToHammersmithTrip, BigDecimal(23.7))

      demoCard must_== expectedTubeEarlCourtToHammersmithCard
      demoCard.balance must_== BigDecimal(23.7)

    }

  }
}